package dingapi

import (
	entity "gitee.com/blizzard1413/ding-api/entity"
	service "gitee.com/blizzard1413/ding-api/service"
)

func NewDingApi(appkey string, appsecret string, AgentId string) entity.DingApi {
	return service.NewDingService(appkey, appsecret, AgentId)
}
