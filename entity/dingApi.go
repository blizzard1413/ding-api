package entity

type DingApi interface {
	//获取用户信息
	GetUserInfo(code string) (user DingUser, err error)
}
