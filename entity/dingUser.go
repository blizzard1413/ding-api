package entity

type DingUser struct {
	Userid            string `json:"userid"`
	DeviceId          string `json:"device_id"`
	Sys               bool   `json:"sys"`
	SysLevel          int    `json:"sys_level"`
	AssociatedUnionid string `json:"associated_unionid"`
	Unionid           string `json:"unionid"`
	Name              string `json:"name"`
}
