package request

import (
	"net/http"
	"net/url"
)

type dingToken struct {
	dingResult
	AccessToken string `json:"access_token"`
	ExpiresIn   uint64 `json:"expires_in"`
}

func GetToken(appkey string, appsecret string) (Token string, err error) {

	params := url.Values{}
	params.Add("appkey", appkey)
	params.Add("appsecret", appsecret)
	req, _ := http.NewRequest(http.MethodGet, "https://oapi.dingtalk.com/gettoken?"+params.Encode(), nil)
	var token dingToken

	err = request(req, &token)

	if err != nil {
		return "", err
	}
	return token.AccessToken, err

}
