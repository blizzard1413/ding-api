package request

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/url"

	entity "gitee.com/blizzard1413/ding-api/entity"
)

type dingUserResult struct {
	dingResult
	entity.DingUser
}

func GetUserInfo(token string, code string) (user entity.DingUser, err error) {
	params := url.Values{}
	params.Add("access_token", token)
	var data = struct {
		Code string `json:"code"`
	}{
		Code: code,
	}
	jdata, _ := json.Marshal(&data)

	body := bytes.NewBuffer(jdata)
	req, _ := http.NewRequest(http.MethodGet, "https://oapi.dingtalk.com/gettoken?"+params.Encode(), body)

	var result dingUserResult

	err = request(req, &token)

	if err != nil {
		return entity.DingUser{}, err
	}
	return result.DingUser, err
}
