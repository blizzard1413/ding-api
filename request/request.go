package request

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type dingResult struct {
	Errcode int    `json:"errcode"`
	Errmsg  string `json:"errmsg"`
}

// 封装的请求函数
func request(req *http.Request, result interface{}) (err error) {
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	resp, err := io.ReadAll(response.Body)
	if err != nil {
		return
	}
	defer response.Body.Close()

	var errResult dingResult

	err = json.Unmarshal(resp, &errResult)
	if err != nil {
		err = fmt.Errorf("unmarshal error %s", string(resp))
		return
	}
	if errResult.Errcode != 0 {
		return fmt.Errorf("%v:%s", req.URL, errResult.Errmsg)
	}

	err = json.Unmarshal(resp, &result)
	if err != nil {
		err = fmt.Errorf("unmarshal error %s", string(resp))
		return
	}

	return
}
