package service

import (
	entity "gitee.com/blizzard1413/ding-api/entity"
)

type DingService struct {
	appkey    string
	appsecret string
	AgentId   string
}

func NewDingService(appkey string, appsecret string, AgentId string) entity.DingApi {
	return &DingService{appkey: appkey, appsecret: appsecret, AgentId: AgentId}
}
