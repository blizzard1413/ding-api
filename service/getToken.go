package service

import (
	request "gitee.com/blizzard1413/ding-api/request"
)

func (d *DingService) getToken() (Token string, err error) {
	Token, err = request.GetToken(d.appkey, d.appsecret)
	return
}
