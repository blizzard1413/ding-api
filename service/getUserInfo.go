package service

import (
	entity "gitee.com/blizzard1413/ding-api/entity"
	request "gitee.com/blizzard1413/ding-api/request"
)

func (d *DingService) GetUserInfo(code string) (user entity.DingUser, err error) {

	var token string

	token, err = d.getToken()

	if err != nil {
		return entity.DingUser{}, err
	}

	user, err = request.GetUserInfo(token, code)

	if err != nil {
		return entity.DingUser{}, err
	}

	return user, err
}
